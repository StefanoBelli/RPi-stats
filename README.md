# RPi-stats
Using LCD 16x2, wiringPi and other stuffs... 


### WiringPi
This project doesn't use the famous python module "RPi.GPIO" but it is developed in C w/ wiringPi, so we can get better performance
Before launching RPi-stats you should install wiringPi shared libraries and headers.

 * http://wiringpi.com/


### What is this?
I am doing this for my usage but I want to share what I am doing. (Probably you already seen something like this)
An LCD 16x2 display (2 rows,16 cols) [4-bit only] shows information about the raspy and I want to implement something new... Need to think about it 

### What infos?
 
 * Uptime 
 * Hostname 
 * Local IP address
 * CPU Usage
 * many other things...

### Project

*Electric scheme(yes I know it is horrible but.. for now)* 
![Electrical scheme](project/rpistats_schem.jpg)

*Breadboard*
![Breadboard](project/rpistats_bb.jpg)

You can also find the fritzing file in project/

### WiringPi Pin Numbering 

 - LCD :: RS: 1
 - LCD :: E : 4
 - LCD :: D7: 5
 - LCD :: D6: 6
 - LCD :: D5: 3
 - LCD :: D4: 2

### How to build?
I created a script for this purpose
You can directly pass make _options_

~~~
$ ./build.sh #builds project (you will get executable in the same dir 
~~~

~~~
$ ./build.sh clean #cleans project (object files and executable)
~~~

~~~
$ ./build.sh install #installs project to /usr/local/bin/rpistats (you must be root)
~~~

~~~
$ ./build.sh -k #launches make -k, instead of make (w/out options)
~~~

### What you need
Basically
 * LCD 15x2 
 * Breadboard 
 * Raspberry Pi
 * WiringPi headers/libraries installed

### Copyleft
This is not something new and innovative, take it and do what you want with it!

