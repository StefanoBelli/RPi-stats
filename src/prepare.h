#ifndef _PREPARE_H
#define _PREPARE_H

struct lcd {
	int rs;
	int e;
	int d7; 
	int d6; 
	int d5; 
	int d4;
};

char error[256];

extern int pbtn_reset;

extern int lcd_prepare();
extern int prepare();

#endif //_PREPARE_H
