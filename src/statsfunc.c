/*
 * Copyleft <(C)
 * Originally written by Stefano Belli
 * 
 * [Viewing] 
 *  -- statsfunc.c
 * [See also]
 *  -- statsfunc.h 
*/
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef __gnu_linux__
#include <sys/sysinfo.h>
#endif //__gnu_linux__

#include "stats.h"
#include "logger.h"


/** GET STATS**/ 
char* rpi_get_uptime()
{
#ifdef __gnu_linux__
  char* fmt = malloc(MAX_BUFFER);
  int days_uptime,hour_uptime,mins_uptime;
  struct sysinfo uptime_sinfo;

  if(sysinfo(&uptime_sinfo) != 0) {
    logger(LOG_ERROR,"rpi_get_uptime():sysinfo() --> Error");
    return "Uptime error!";
  }
  
  days_uptime = uptime_sinfo.uptime / 86400;
  hour_uptime = (uptime_sinfo.uptime / 3600) - (days_uptime * 24);
  mins_uptime = (uptime_sinfo.uptime / 60) - (days_uptime * 1440) - (hour_uptime * 60);

  sprintf(fmt,"%dd:%dh:%dm",days_uptime,hour_uptime,mins_uptime);

  return fmt;
#else
  char* fmt = malloc(MAX_BUFFER);
  strcpy(fmt,"Not supported by your OS!");

  return fmt;
#endif //__gnu_linux__
}

char* rpi_get_procs()
{
#ifdef __gnu_linux__
  char *fmt = malloc(MAX_BUFFER);
  struct sysinfo proc_sinfo;
  if(sysinfo(&proc_sinfo) != 0) {
    logger(LOG_ERROR,"rpi_get_procs():sysinfo() --> Error");
    return "Procs error!";
  }

  sprintf(fmt,"%d",proc_sinfo.procs);

  return fmt;
#else
  char* fmt = malloc(MAX_BUFFER);
  strcpy(fmt,"Not supported by your OS!");

  return fmt;
#endif //__gnu_linux__
}

char* rpi_get_ram()
{
#ifdef __gnu_linux__ 
  char* fmt = malloc(MAX_BUFFER);
  struct sysinfo ram_sinfo;

  if(sysinfo(&ram_sinfo) != 0) {
    logger(LOG_ERROR,"rpi_get_ram():sysinfo() --> Error");
    return "RAM Error!";
  }

  sprintf(fmt,"%lu / %lu MB",ram_sinfo.freeram/MEGABYTE,ram_sinfo.totalram/MEGABYTE);
  return fmt;
#else
  char* fmt = malloc(MAX_BUFFER);
  strcpy(fmt,"Not supported by your OS!");

  return fmt;
#endif //__gnu_linux__
}

char* rpi_get_swap()
{
#ifdef __gnu_linux__ 
  char* fmt = malloc(MAX_BUFFER);
  struct sysinfo swap_sinfo;

  if(sysinfo(&swap_sinfo) != 0) {
    logger(LOG_ERROR,"rpi_get_swap():sysinfo() --> Error");
    return "Swap Error!";
  }

  sprintf(fmt,"%lu / %lu MB",swap_sinfo.freeswap/MEGABYTE,swap_sinfo.totalswap/MEGABYTE);

  return fmt;
#else
  char* fmt = malloc(MAX_BUFFER);
  strcpy(fmt,"Not supported by your OS!");

  return fmt;
#endif //__gnu_linux__
}
/** END GET STATS **/

/** GET STATE **/
void* rpi_ctl_btn_reset(void* __data_ctl)
{
  struct ctl_data *data_ctl = (struct ctl_data*)__data_ctl;
  
  return NULL;
}
/** END GET STATE **/
