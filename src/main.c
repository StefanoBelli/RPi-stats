/*
 * Copyleft <(C)
 * Originally written by Stefano Belli
 * 
 * This software is not licensed under any license, 
 * use it the way !YOU! want.
 * 
 * GitHub: https://github.com/StefanoBelli
 * RPi-stats GitHub repository: https://github.com/StefanoBelli/RPi-stats
 * GooglePlus: https://plus.google.com/+StefanoBelli
 * Twitter: @S73FYH4CK (Yes, stupid nick)
 * mailto: stefano9913@gmail.com 
 *
 * [Viewing]
 *  -- main.c
 */
#include <stdlib.h>

#include "misc.h"
#include "logger.h"
#include "prepare.h"
#include "stats.h"

int main()
{
	int lcdfd;
	
	set_logpath(".rpistats.log");

	logger(LOG_NEWSESSION,"RPiStats New Session Started!");

	create_process();
	
	if(prepare() == 0) {
		logger(LOG_SUCC,"prepare() done");
	} else {
		logger(LOG_ERROR,"prepare() failed");
		exit(ERROR_PREPARE);
	}
	
	if((lcdfd=lcd_prepare()) != ERROR_LCDINIT) {
		logger(LOG_SUCC,"lcd_prepare() done");
	} else {
		logger(LOG_ERROR,"lcd_prepare() failed");
		exit(ERROR_LCDINIT);
	}

	handle(lcdfd,pbtn_reset);
	
	return 0;
}

