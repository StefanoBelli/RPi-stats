/*
 * Copyleft <(C)
 * Originally written by Stefano Belli
 * 
 * [Viewing] 
 *  -- stats.c
 * [See also]
 *  -- stats.h 
*/
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <wiringPi.h>
#include <lcd.h>

#include "stats.h"
#include "statsfunc.h"
#include "misc.h"
#include "logger.h"

//used to convert some data to Mega Byte 
unsigned long MEGABYTE = 1024 * 1024;

//This matrix contains data names to show 
char names[NSTATS][MAX_BUFFER] = {
  "Uptime",
  "Processes",
  "RAM(Free/Tot)",
  "Swap(Free/Tot)"
};

//strfptr_t is a function pointer {aka char* (*strfptr_t)() }
//points rpi_get* functions which returns text to show 
strfptr_t get_stats[NSTATS] = {
  rpi_get_uptime,
  rpi_get_procs,
  rpi_get_ram,
  rpi_get_swap
};

//ctlfptr_t is a function pointer {aka void* (*ctlfptr_t)(pthread_t,int)}
//points to rpi_ctl* functions which checks some state
ctlfptr_t get_state[NSTATE] = {
  rpi_ctl_btn_reset
};

/** Main Handling function **/
void handle(long lcd_fd, int pushb_reset) //extern 
{ 
  int index;
  pthread_t data_thr, ctl_thr;
  struct ctl_data data_ctl;
  
  if ( pthread_create(&data_thr,NULL,__rpi_get_data,(void*)lcd_fd) < 0 ) {
    logger(LOG_ERROR, "pthread_create returned < 0");
    exit(1);
  }
  
  data_ctl.dtid=data_thr;
  data_ctl.fdlcd=lcd_fd;
  data_ctl.pushbutton_reset=pushb_reset; //todo
  
  for(index=0;index<=NSTATE_COUNT;index++) {
    if( pthread_create(&ctl_thr,NULL,get_state[index],(void*)&data_ctl) < 0 ) {
      logger(LOG_ERROR, "pthread_create returned < 0");
      exit(1);
    }
  }

  pthread_join(data_thr,NULL);
  
  for(index=0;index<=NSTATE_COUNT;index++) {
    pthread_join(ctl_thr,NULL);
  }
}
/*+----------------------------------------+*/

void* __rpi_get_data(void* fd_lcd)
{
  //mutex
  int index,lcd_fd;
  
  lcd_fd=(int)fd_lcd;
   
  while(1) {

    //date&time
    for(index=0;index<=3;index++) {
      lcdClear(lcd_fd);
      lcdPosition(lcd_fd,0,0);
      lcdPuts(lcd_fd,get_time());
      sleep(1);
    }

	  //get stats!
    for(index=0;index<=NSTATS_COUNT;index++) {
      char* rpi_get_stat=get_stats[index]();
      lcdClear(lcd_fd);
      lcdPosition(lcd_fd,0,0);
      lcdPuts(lcd_fd,names[index]);
      lcdPosition(lcd_fd,0,1);
      lcdPuts(lcd_fd,rpi_get_stat);
      // every function allocates MAX_BUFFER to heap, then free that alloc.
      free(rpi_get_stat);  
      sleep(3);
    }
  }
}
