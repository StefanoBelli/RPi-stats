#ifndef _MISC_H
#define _MISC_H

#define ERROR_FORK 1
#define ERROR_SETSID 2
#define ERROR_PREPARE -3
#define ERROR_WPISETUP -1
#define ERROR_LCDINIT -2

extern void create_process();
extern char* get_time();

#endif //_MISC_H
