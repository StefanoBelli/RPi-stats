#ifndef _LOGGER_H
#define _LOGGER_H

#define LOG_ERROR 1
#define LOG_SUCC 0
#define LOG_WARN 2
#define LOG_NEWSESSION 3

extern void logger(int mode,const char* _msg);
extern void set_logpath(const char* __fname);

extern char logpath[256];

#endif //_LOGGER_H
