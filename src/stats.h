#ifndef _STATS_H
#define _STATS_H

#define NSTATS 4
#define NSTATS_COUNT NSTATS-1
#define NSTATE 1
#define NSTATE_COUNT NSTATE-1
#define MAX_BUFFER 256

struct ctl_data {
    pthread_t dtid;
    int fdlcd;
    int pushbutton_reset;
};

unsigned long MEGABYTE;

typedef char* (*strfptr_t)();
typedef void* (*ctlfptr_t)(void*);

char names[NSTATS][MAX_BUFFER];

strfptr_t get_stats[NSTATS];
ctlfptr_t get_state[NSTATE];

extern void handle(long lcd_fd,int pushb_reset);

void* __rpi_get_data(void* fd_lcd);

#endif //_STATS_H
