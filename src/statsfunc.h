#ifndef __STATS_FUNC_H
#define __STATS_FUNC_H

/*get stats*/
char* rpi_get_uptime();
char* rpi_get_procs();
char* rpi_get_ram();
char* rpi_get_swap();
/*end get stats*/

/*get state*/
void* rpi_ctl_btn_reset(void* __data_ctl);
/*end get state*/

#endif //__STATS_FUNC_H