/*
 * Copyleft <(C)
 * Originally written by Stefano Belli
 * 
 * [Viewing]
 *  -- misc.c
 * [See also]
 *  -- misc.h
 */
#include<stdlib.h>
#include<unistd.h>
#include<errno.h>
#include<sys/types.h>
#include<stdio.h>
#include<string.h>
#include<time.h>

#include "misc.h"
#include "logger.h"

void create_process() //Background 
{
	int sid;
	pid_t proc_id;
	char error[256], fmt[256];

	proc_id=fork();
	
	switch(proc_id) {
		case 0:
		sprintf(fmt,"PID: %d :: Parent PID: %d",getpid(),getppid());
		logger(LOG_SUCC,fmt);
		break;
		
		case -1:
		sprintf(error,"fork() failed: %s",strerror(errno));
		logger(LOG_ERROR,error);
		exit(ERROR_FORK);
		break;

		default:
		exit(0);
		break;
	}
	
	sid=setsid();
	
	switch(sid) {
		case 0:
		break;
		
		case -1:
		sprintf(error,"setsid() failed: %s",strerror(errno));
		logger(LOG_ERROR,error);
		exit(ERROR_SETSID);
		break;
	}
	
	close(0);
	close(1);
	close(2);
	
	logger(LOG_SUCC,"create_process() successfully done!");
}

char* get_time()
{
	time_t ttime;
	struct tm *tinfo;
	
	time(&ttime);
	tinfo=localtime(&ttime);
	
	return asctime(tinfo);
}

	
