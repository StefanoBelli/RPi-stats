/*
 * Copyleft <(C)
 * Originally written by Stefano Belli
 * 
 * [Viewing]
 *  -- logger.c
 * [See also]
 *  -- logger.h
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#include "logger.h"
#include "misc.h"

char logpath[256];

void set_logpath(const char* __fname)
{
	char final_fname[256];
	sprintf(final_fname,"/%s",__fname);
	strcpy(logpath,getenv("HOME"));
	strcat(logpath,final_fname);
}	
	
void logger(int mode,const char* _msg) 
{	
	FILE* logfile;
	if((logfile=fopen(logpath,"a+")) != NULL) {
		char* _current_time = get_time();
		
		if (mode == LOG_SUCC) {
			fprintf(logfile,"=%s [SUCCESS] %s\n",_current_time,_msg);
		} else if(mode == LOG_ERROR) {
			fprintf(logfile,"=%s [ERROR] %s\n",_current_time,_msg);
		} else if(mode == LOG_WARN) {
			fprintf(logfile,"=%s [WARN] %s\n",_current_time,_msg);
		} else if(mode == LOG_NEWSESSION) {
			fprintf(logfile,"+--------------------%s--------------------+\n",_msg);
		} else {
			fprintf(logfile,"=%s [LOGGING][ERROR]Non-valid logging mode!\n",_current_time);
		}
		fclose(logfile);
	} else {
		fclose(logfile);
	}
}
