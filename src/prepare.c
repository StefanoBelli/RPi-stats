/*
 * Copyleft <(C)
 * Originally written by Stefano Belli
 * 
 * [Viewing]
 * -- prepare.c
 * [See also]
 * -- prepare.h
 */
#include<wiringPi.h>
#include<lcd.h>
#include<errno.h>
#include<string.h>
#include<stdio.h>

#include "prepare.h"
#include "logger.h"
#include "misc.h"

char error[256];
int pbtn_reset=7;	

int prepare()
{ 
	if(wiringPiSetup() != 0) {
		sprintf(error,"wiringPiSetup() failed: %s",strerror(errno));
		logger(LOG_ERROR,error);
		return ERROR_WPISETUP;
	}
  
  //pinMode(pbtn_reset,OUTPUT);
	
	return 0;
}

int lcd_prepare()
{
	// Based on wiringPi's pin numering
	struct lcd lcd_data;
	lcd_data.rs=1;
	lcd_data.e=4;
	lcd_data.d7=5;
	lcd_data.d6=6;
	lcd_data.d5=3;
	lcd_data.d4=2;
	
	int lcdfd;
	if((lcdfd=lcdInit(2,16,4,lcd_data.rs,lcd_data.e,lcd_data.d4,lcd_data.d5,lcd_data.d6,lcd_data.d7,0,0,0,0)) != 0) {
		sprintf(error,"lcdInit() failed: %s",strerror(errno));
		logger(LOG_ERROR,error);
		return ERROR_LCDINIT;
	}
	
	return lcdfd;
}
