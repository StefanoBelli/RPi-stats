#!/bin/bash

#
# Copyleft <(C)
# Originally written by Stefano Belli
#
# [Viewing]
#  -- build.sh 
# [See also]
#  -- src/Makefile
#

# Build script for RPi-stats!
# ./build.sh # and its done!
# ./build.sh clean #to clean project
# You can also directly type make options 
# ./build.sh -j3 # launches make -j3 

if which make >>/dev/null 2>/dev/null;
then
		echo "[OK] Make found"
else
		echo "[ERROR] Make not found"
		exit 1
fi

if which gcc >>/dev/null 2>/dev/null;
then
		echo "[OK] GCC Compiler found"
else
		echo "[ERROR] GCC Compiler not found"
		exit 1
fi

echo "[WARN] This project uses wiringPi"
echo "[INFO] Changing directory"

if [ -d src ];
then
		cd src
else
		echo "[ERROR] src/ directory not found"
		exit 1
fi

echo "[INFO] Launching make..."
make $@
if [ $? -eq 0 ];
then
		echo "[INFO] You should find binary here"
else
		echo "[ERROR] Something went wrong"
		exit 2
fi

cd ..

for i in $@; 
do
	   [[ $i == "clean" ]] || continue
		   if [ -f rpistats ];
		   then
				 rm rpistats
		   fi
done	

